<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp777');

/** MySQL database username */
define('DB_USER', 'wp777');

/** MySQL database password */
define('DB_PASSWORD', '1SVPS09x.-');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fnafqsdrmwiwhaz7dr8koy1q5hxkg0rx6qcmd0cbfxogzovpooxpf34lrz50s4eh');
define('SECURE_AUTH_KEY',  't2monjr3ecyz1nqq0ixdl4jjzxki6f3kavcjv3jb4ynqrckegom6j1zkrto6t7yl');
define('LOGGED_IN_KEY',    'lnuenukecnlwlatyc2effxo3wqivhmcfwvenwllpmhsxuz8fbdcdsinqiv6daznt');
define('NONCE_KEY',        'pb2vxlayczztc0zebar0z6faq1dg4rt8a2qe6oun4npt1urhqkajydfy7uaugnad');
define('AUTH_SALT',        'p1htm0hrmvuqsnwsh0k9aqxifzvwmljubntvsnwqh7mtxg5t5j12mdr0oepunwnm');
define('SECURE_AUTH_SALT', 'vs2eer28eceg33crwmuwdacqzekcm7mfv5gi3nkvukkgsuh4rorbzyqurlfxkhgh');
define('LOGGED_IN_SALT',   '6rlbbjddjxci8ijqpjuanli3ypfgn50ty5mvaueum6lnxbt5phqlpmaf541ngrkd');
define('NONCE_SALT',       'mos4xwhlcuqwlssvv89mggcb8eu24npqyqhlr7nl5uwwlg7jvxgila3kvazyrv4p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
