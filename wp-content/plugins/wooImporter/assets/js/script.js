String.prototype.replaceAllTags = function(tag){
    var div = document.createElement('div');
    div.innerHTML = this;
    var scripts = div.getElementsByTagName(tag);
    var i = scripts.length;
    while (i--) {
        scripts[i].parentNode.removeChild(scripts[i]);
    }
    return div.innerHTML;
}

jQuery(function () {
    
    jQuery("#wpeae-do-filter").click(function () {
        jQuery("#wpeae-search-form").find("#reset").val("1");
        jQuery("#wpeae-search-form").submit();
        return true;
    });


    jQuery("#wpeae-search-form").submit(function () {
        jQuery("input[name='_wp_http_referer']").attr("disabled", "disabled");
        jQuery("input[name='_wpnonce']").attr("disabled", "disabled");

        jQuery(this).find(":input").filter(function () {
            return !this.value;
        }).attr("disabled", "disabled");
        return true;
    });

    jQuery("#wpeae-goods-table").on("click", ".select_image", function () {
        return true;
    });

    jQuery("body").on("click", ".wpeae_select_image img", function () {
        var id = jQuery(this).parent().parent().find('.item_id').val();
        var new_image = jQuery(this).attr('src');

        jQuery(this).parent().parent().find("img.sel").removeClass("sel");
        jQuery(this).addClass("sel");

        jQuery("#wpeae-goods-table").find('tr').each(function () {
            var row_id = jQuery(this).attr('id');
            if (row_id === id) {
                jQuery(this).find('.column-image img').attr('src', new_image);
            }
        });


        var data = {'action': 'wpeae_select_image', 'id': id, 'image': new_image};

        jQuery.post(ajaxurl, data, function (response) {
        });
        return false;
    });
    jQuery("body").on("click", ".show_preview", function () {
        jQuery(this).parents(".edit_description_dlg").find('.description_preview').show();
        jQuery(this).parents(".edit_description_dlg").find('.edit_description').show();
        jQuery(this).parents(".edit_description_dlg").find('.description').hide();
        jQuery(this).parents(".edit_description_dlg").find('.show_preview').hide();
        jQuery(this).parents(".edit_description_dlg").find('.save_description').hide();
        
        var text = jQuery(this).parents(".edit_description_dlg").find('.description').val();
        jQuery(this).parents(".edit_description_dlg").find('.description_preview').html(text.replaceAllTags('script'));
    });
    
    jQuery("body").on("click", ".edit_description", function () {
        jQuery(this).parents(".edit_description_dlg").find('.description_preview').hide();
        jQuery(this).parents(".edit_description_dlg").find('.edit_description').hide();
        jQuery(this).parents(".edit_description_dlg").find('.description').show();
        jQuery(this).parents(".edit_description_dlg").find('.show_preview').show();
        jQuery(this).parents(".edit_description_dlg").find('.save_description').show();
    });
    
    jQuery("#wpeae-goods-table").on("click", ".edit_btn", function () {
        var block = jQuery(this).parents(".block_field");
        var text = jQuery(block).find(".field_text").html();

        jQuery(block).find(".field_edit").val(text);

        jQuery(block).find(".field_text").hide();
        jQuery(block).find(".edit_btn").hide();
        jQuery(block).find(".field_edit").show();
        jQuery(block).find(".save_btn").show();
        jQuery(block).find(".cancel_btn").show();

        return false;
    });
    jQuery("#wpeae-goods-table").on("click", ".save_btn", function () {
        var id = jQuery(this).parents('tr').attr('id');
        var block = jQuery(this).parents(".block_field");

        var field_code = jQuery(block).find(".field_code").val();
        var text = jQuery(block).find(".field_edit").val();

        jQuery(block).find(".field_text").show();
        jQuery(block).find(".edit_btn").show();
        jQuery(block).find(".field_edit").hide();
        jQuery(block).find(".save_btn").hide();
        jQuery(block).find(".cancel_btn").hide();

        jQuery(block).find(".field_text").html(text);

        var data = {'action': 'wpeae_edit_goods', 'id': id,
            'field': (field_code.lastIndexOf('user_', 0) === 0) ? field_code : ('user_' + field_code),
            'value': text};

        jQuery.post(ajaxurl, data, function (response) {});

        return false;
    });

    jQuery("#wpeae-goods-table").on("click", ".cancel_btn", function () {
        var block = jQuery(this).parents(".block_field");

        jQuery(block).find(".field_text").show();
        jQuery(block).find(".edit_btn").show();
        jQuery(block).find(".field_edit").hide();
        jQuery(block).find(".save_btn").hide();
        jQuery(block).find(".cancel_btn").hide();
        return false;
    });

    jQuery("#wpeae-goods-table").on("click", ".moredetails", function () {
        var block = jQuery(this).parent();
        var curr_row = jQuery(this).parents("tr");
        var id = jQuery(this).parents("tr").attr('id');

        jQuery(block).html("<i>loading...</i> | ");

        var edit_fields = '';
        jQuery(curr_row).find(".block_field").each(function () {
            var field_code = jQuery(this).find(".field_code").val();
            if (jQuery(this).hasClass('edit')) {
                edit_fields += (edit_fields.length > 0 ? ',' : '') + field_code;
            }
        });

        var data = {'action': 'wpeae_load_details', 'id': id, 'edit_fields': edit_fields};

        jQuery.post(ajaxurl, data, function (response) {
            jQuery(block).html('<i>Details loaded</i> | ');

            var json = jQuery.parseJSON(response);

            if (json.state = 'ok') {
                jQuery(curr_row).find("#select-image-dlg-" + json.goods.type + "-" + json.goods.external_id).html(json.images_content);
                jQuery(curr_row).find("#edit-description-dlg-" + json.goods.type + "-" + json.goods.external_id).html(json.description_content);

                if (jQuery(curr_row).find(".seller_url_block").is(':hidden')) {
                    jQuery(curr_row).find(".seller_url_block").find('a').attr('href', json.goods.seller_url);
                    jQuery(curr_row).find(".seller_url_block").show();
                }

                jQuery(curr_row).find(".block_field").each(function () {
                    var field_code = jQuery(this).find(".field_code").val();
                    jQuery(this).find('.field_text').html(json.goods[field_code]);
                    jQuery(this).find('.field_text').show();
                    jQuery(this).find('.edit_btn').show();
                });
                //console.log('[' + json.state + ']message: ', json.message);
            } else {
                console.log('[' + json.state + ']message: ', json.message);
            }
        });

        return false;
    });

    jQuery("#wpeae-goods-table").on("click", ".post_import", function () {
        var id = jQuery(this).parents("tr").attr('id');
        var block = jQuery(this).parent();
        jQuery(block).html('<i>Posting...</i> | ');

        var data = {'action': 'wpeae_import_goods', 'id': id};

        jQuery.post(ajaxurl, data, function (response) {
            //console.log('response: ', response);
            var json = jQuery.parseJSON(response);
            //console.log('result: ', json);

            if (json.state === 'error') {
                jQuery(block).html('<i>Posting error</i> | ');
                console.log(json);
            } else {
                jQuery(this).parents("tr").find('input[type=checkbox]').attr('disabled', 'disabled');
                jQuery(block).html('<i>Posted</i>');
                jQuery(block).parents('.row-actions').find('.schedule_import').remove();
                jQuery(block).parents("tr").find('input[type=checkbox]').attr('disabled', 'disabled');
            }
        });

        return false;
    });
    
    jQuery("#wpeae-goods-table").on("click", "#doaction,#doaction2", function () {
        var check_action = (jQuery(this).attr('id')=='doaction')?jQuery('#bulk-action-selector-top').val():jQuery('#bulk-action-selector-bottom').val();
        jQuery("#wpeae-goods-table .import_process_loader").html("");
        if(check_action == 'import'){
            var num_to_import = jQuery("#wpeae-goods-table input.gi_ckb:checked").length;
            
            if(num_to_import > 0){
                jQuery("#wpeae-goods-table .import_process_loader").html("Process import 0 of "+num_to_import+".");
                var import_cnt = 0;
                var import_error_cnt = 0;
                var import_cnt_total = 0;
                jQuery("#wpeae-goods-table input.gi_ckb:checked").each(function () {
                    var id = jQuery(this).parents("tr").attr('id');
                    var block = jQuery(this).parents("tr").find('.row-actions .import');

                    var data = {'action': 'wpeae_import_goods', 'id': id};

                    jQuery.post(ajaxurl, data, function (response) {
                        //console.log('response: ', response);
                        var json = jQuery.parseJSON(response);
                        //console.log('result: ', json);

                        if (json.state === 'error') {
                            jQuery(block).html('<i>Posting error</i> | ');
                            console.log(json);
                            import_error_cnt++;
                        } else {
                            jQuery(block).html('<i>Posted</i>');
                            jQuery(block).parents('.row-actions').find('.schedule_import').remove();
                            jQuery(block).parents("tr").find('input[type=checkbox]').attr('disabled', 'disabled');
                            jQuery(block).parents("tr").find('input[type=checkbox]').removeAttr('checked');
                            import_cnt++;
                        }
                        import_cnt_total++;
                        jQuery("#wpeae-goods-table .import_process_loader").html("Process import "+import_cnt+" of "+num_to_import+". Errors: "+import_error_cnt+".");
                        
                        if(import_cnt_total == num_to_import){
                            jQuery("#wpeae-goods-table .import_process_loader").html("Import complite! Result imported: "+import_cnt+"; errors: "+import_error_cnt+".");    
                        }
                    });
                });    
            }
        }
               
        return false;
    });

    jQuery(".schedule_post_date").datetimepicker({
        format: 'm/d/Y H:i',
        step: 10,
        onSelectTime: function (dateText, input) {
            var id = jQuery(input).parents("tr").attr('id');
            var block = jQuery(input).parent();
            
            jQuery(block).html("<i>Process...</i>");

            var data = {'action': 'wpeae_schedule_import_goods', 'id': id, 'time': jQuery(input).val()};

            jQuery.post(ajaxurl, data, function (response) {
                var json = jQuery.parseJSON(response);
                if (json.state == 'error') {
                    jQuery(block).html("<i>Schedule post error</i>");
                } else {
                    jQuery(block).html("<i>Will be post on " + json.time + "</i>");
                }
            });


        }
    });

    jQuery("#wpeae-goods-table").on("click", ".schedule_post_import", function () {
        jQuery(this).prev().datetimepicker('show');
        return false;
    });

    jQuery(".upload_image").click(function () {
        jQuery("#upload_product_id").val(jQuery(this).parents('tr').attr('id'));
        return true;
    });
    
    
    
    jQuery(".edit_desc_action").click(function () {
        var id = jQuery(this).parents("tr").attr('id');
        
        jQuery('#edit_desc_dlg').empty();
        jQuery('#edit_desc_dlg').append( '<div><h2>Edit description</h2><div id="edit_desc_content">Loading...</div></div>' );
        
        var data = {'action': 'wpeae_description_editor', 'id': id};
        jQuery.post(ajaxurl, data, function (response) {
            //console.log('response: ', response);
            jQuery('body').find('#edit_desc_content').html(response);
        });

        return true;
    });
    
    function get_tinymce_content(id) {
        var content;
        var inputid = id;
        var editor = tinyMCE.get(inputid);
        var textArea = jQuery('textarea#' + inputid);    
        if (textArea.length>0 && textArea.is(':visible')) {
            content = textArea.val();        
        } else {
            content = editor.getContent();
        }    
        return content;
    }
    
    jQuery("body").on("click", ".save_description", function () {
        var save_btn = this;
        jQuery(save_btn).val('Saving...');
        jQuery(save_btn).prop('disabled', true);
        
        var id = jQuery(this).parent().find('.item_id').val();
        var editor_id = jQuery(this).parent().find('.editor_id').val();
        var data = {'action': 'wpeae_edit_goods', 'id': id, 'field': 'user_description', 'value': get_tinymce_content(editor_id)/*jQuery(this).parent().find('textarea').val()*/};
        jQuery.post(ajaxurl, data, function (response) {
            jQuery(save_btn).val('Save description');
            jQuery(save_btn).prop('disabled', false);
            tb_remove();
        });
    });
    
    if (!!jQuery.prototype.ajaxForm){
        var options = {target: '', beforeSubmit: showRequest, success: showResponse, url: ajaxurl};
        jQuery('#image_upload_form').ajaxForm(options);
    }else{
        console.log('Warnign! ajaxForm is not suported by your theme');
    }
    
    jQuery("#image_upload_form").on("change", "#upload_image", function () {
        jQuery("#image_upload_form").find('#upload_progress').html('');
    });
    
});

function showRequest(formData, jqForm, options) {
    if(jQuery(jqForm).find("#upload_image").val() !== ''){
        jQuery(jqForm).find('#upload_progress').html('Sending...');
        jQuery(jqForm).find('input[name="submit-ajax"]').attr("disabled", "disabled");
        return true;
    }else{
        jQuery(jqForm).find('#upload_progress').html('<font color="red">Please select a file first</font>');
        jQuery(jqForm).find('input[name="submit-ajax"]').removeAttr("disabled");
        return false;
    }
    
}
function showResponse(responseText, statusText, xhr, $form) {
    var json = jQuery.parseJSON(responseText);
    if (json.state == 'ok') {
        jQuery("#wpeae-goods-table").find('tr').each(function () {
            var row_id = jQuery(this).attr('id');
            if (row_id === json.goods.type + "#" + json.goods.external_id) {
                jQuery(this).find('.column-image img').attr('src', json.cur_image);
            }
        });

        jQuery("#select-image-dlg-" + json.goods.type + "-" + json.goods.external_id).html(json.images_content);
    } else {
        console.log(json.state + "; " + json.message);
    }


    jQuery($form).find('input[name="submit-ajax"]').removeAttr("disabled");
    jQuery($form).find('#upload_image').val('');
    jQuery($form).find('#upload_product_id').val('');
    jQuery($form).find('#upload_progress').html('');

    tb_remove();
}


