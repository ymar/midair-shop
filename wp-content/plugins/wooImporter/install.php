<?php
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

if (!function_exists('wpeae_install')) {
    function wpeae_install() {
	/** @var wpdb $wpdb */
        global $wpdb;

        add_option( 'wpeae_price_multiplier', '1', '', 'no' );
        add_option( 'wpeae_per_page', '25', '', 'no' );
        add_option( 'wpeae_default_type', 'simple', '', 'no' );
        add_option( 'wpeae_default_status', 'publish', '', 'no' );
        add_option( 'wpeae_default_shipment_charges', '0.00', '', 'no' );
        add_option( 'wpeae_use_source_shipment', true, '', 'no' );
        add_option( 'wpeae_import_attributes', true, '', 'no' );
        add_option( 'wpeae_use_affiliate_url', true, '', 'no' );
        add_option( 'wpeae_price_auto_update', false, '', 'no' );
        add_option( 'wpeae_price_auto_update_period', 'daily', '', 'no' );
        add_option( 'wpeae_currency_conversion_factor', '1', '', 'no' );
        
        $price_auto_update = get_option('wpeae_price_auto_update', false);
        if($price_auto_update){
            wp_schedule_event( time(), get_option('wpeae_price_auto_update_period', 'daily'), 'wpeae_update_price_event');
        }else{
            wp_clear_scheduled_hook('wpeae_update_price_event');
        }
        wp_schedule_event( time(), 'hourly', 'wpeae_schedule_post_event' );
        
        $charset_collate = '';
        if (!empty($wpdb->charset)) {
            $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
        }
        if (!empty($wpdb->collate)) {
            $charset_collate .= " COLLATE {$wpdb->collate}";
        }

        $table_name = $wpdb->prefix . WPEAE_TABLE_GOODS;
        $sql = "CREATE TABLE IF NOT EXISTS {$table_name} (" .
                "`type` VARCHAR(100) NOT NULL," .
                "`external_id` VARCHAR(100) NOT NULL," .
                "`image` VARCHAR(1024) NULL DEFAULT NULL," .
                "`detail_url` VARCHAR(1024) NULL DEFAULT NULL," .
                "`seller_url` VARCHAR(1024) NULL DEFAULT NULL," .
                "`photos` TEXT NULL," .
                "`title` VARCHAR(1024) NULL DEFAULT NULL," .
                "`subtitle` VARCHAR(1024) NULL DEFAULT NULL," .
                "`description` MEDIUMTEXT NULL," .
                "`keywords` VARCHAR(1024) NULL DEFAULT NULL," .
                "`price` VARCHAR(50) NULL DEFAULT NULL," .
                "`ship` VARCHAR(50) NULL DEFAULT NULL," .
                "`total_price` VARCHAR(50) NULL DEFAULT NULL," .
                "`curr` VARCHAR(50) NULL DEFAULT NULL," .
                "`ship_to_locations` VARCHAR(1024) NULL DEFAULT NULL," .
                "`category_id` INT NULL DEFAULT NULL," .
                "`category_name` VARCHAR(1024) NULL DEFAULT NULL," .
                "`link_category_id` INT NULL DEFAULT NULL," .
                "`attribute` TEXT NULL," .
                "`condition` VARCHAR(256) NULL DEFAULT NULL," .
                "`validTime` VARCHAR(1024) NULL DEFAULT NULL," .
                "`user_image` VARCHAR(1024) NULL DEFAULT NULL," .
                "`user_photos` TEXT NULL," .
                "`user_title` VARCHAR(1024) NULL DEFAULT NULL," .
                "`user_subtitle` VARCHAR(1024) NULL DEFAULT NULL," .
                "`user_description` MEDIUMTEXT NULL," .
                "`user_keywords` VARCHAR(1024) NULL DEFAULT NULL," .
                "`user_price` VARCHAR(1024) NULL DEFAULT NULL," .
                "`user_ship` VARCHAR(1024) NULL DEFAULT NULL," .
                "`user_schedule_time` DATETIME NULL DEFAULT NULL," .
                "PRIMARY KEY (`type`, `external_id`)" .
                ") {$charset_collate} ENGINE=InnoDB;";
	dbDelta($sql);
        
        $table_name = $wpdb->prefix . WPEAE_TABLE_ACCOUNT;
        $sql = "CREATE TABLE IF NOT EXISTS {$table_name} (" .
                "`id` int(20) unsigned NOT NULL AUTO_INCREMENT," .
                "`name` VARCHAR(1024) NOT NULL," .
                "`data` text DEFAULT NULL," .
                "PRIMARY KEY (`id`)" .
                ") {$charset_collate} ENGINE=InnoDB;";
	dbDelta($sql);

        foreach (wpeae_get_api_list() as /* @var $api WPEAE_AbstractConfigurator */ $api) {
            $api->install();
        }
    }
}

if (!function_exists('wpeae_uninstall')) {
    function wpeae_uninstall() {
	/** @var wpdb $wpdb */
        global $wpdb;

        delete_option( 'wpeae_price_multiplier' );
        delete_option( 'wpeae_per_page', '10' );
        delete_option( 'wpeae_default_type' );
        delete_option( 'wpeae_default_status' );
        delete_option( 'wpeae_default_shipment_charges');
        delete_option( 'wpeae_use_source_shipment');
        delete_option( 'wpeae_import_attributes');
        delete_option( 'wpeae_use_affiliate_url');
        delete_option( 'wpeae_price_auto_update');
        delete_option( 'wpeae_price_auto_update_period');
        delete_option( 'wpeae_currency_conversion_factor');

        wp_clear_scheduled_hook('wpeae_schedule_post_event');
        wp_clear_scheduled_hook('wpeae_update_price_event');

        $sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . WPEAE_TABLE_GOODS . ";";
        $wpdb->query($sql);
        
        $sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . WPEAE_TABLE_ACCOUNT . ";";
        $wpdb->query($sql);
        
        foreach (wpeae_get_api_list() as /* @var $api WPEAE_AbstractConfigurator */ $api) {
            $api->uninstall();
        }
    }
}