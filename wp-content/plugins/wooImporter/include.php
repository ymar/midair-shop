<?php

date_default_timezone_set('GMT');

if (!defined('WPEAE_NAME')) {
    define('WPEAE_NAME', 'WooImporter');
}

if (!defined('WPEAE_TABLE_GOODS')) {
    define('WPEAE_TABLE_GOODS', 'wpeae_goods');
}

if (!defined('WPEAE_TABLE_ACCOUNT')) {
    define('WPEAE_TABLE_ACCOUNT', 'wpeae_account');
}

if (!defined('WPEAE_NO_IMAGE_URL')) {
    define('WPEAE_NO_IMAGE_URL', 'http://pics.ebaystatic.com/aw/pics/express/icons/iconPlaceholder_96x96.gif');
}

include_once(dirname(__FILE__) . '/includes/api/WPEAE_Goods.php');
include_once(dirname(__FILE__) . '/includes/api/WPEAE_AbstractAccount.php');
include_once(dirname(__FILE__) . '/includes/api/WPEAE_AbstractLoader.php');
include_once(dirname(__FILE__) . '/includes/api/WPEAE_AbstractConfigurator.php');
include_once(dirname(__FILE__) . '/includes/api/WPEAE_WooCommerce.php');

include_once(dirname(__FILE__) . '/includes/WPEAE_DashboardPage.php');
include_once(dirname(__FILE__) . '/includes/WPEAE_SettingsPage.php');

include_once(dirname(__FILE__) . '/includes/WPEAE_Ajax.php');

$WPEAE_GLOBAL_API_LIST = array();

if (!function_exists('wpeae_add_api')) {

    function wpeae_add_api($api_configurator) {
        global $WPEAE_GLOBAL_API_LIST;
        $WPEAE_GLOBAL_API_LIST[] = $api_configurator;
    }

}

if (!function_exists('wpeae_add_api_filter')) {

    function wpeae_add_api_filter($configurator, $id, $name, $order = 1000, $config = array()) {
        if ($configurator instanceof WPEAE_AbstractConfigurator) {
            $configurator->add_filter($id, $name, $order, $config);
        }
    }

}

if (!function_exists('wpeae_remove_api_filter')) {

    function wpeae_remove_api_filter($configurator, $id) {
        if ($configurator instanceof WPEAE_AbstractConfigurator) {
            $configurator->remove_filter($id);
        }
    }

}

/* include api modules */
foreach (glob(WPEAE_ROOT_PATH . 'includes/api/*', GLOB_ONLYDIR) as $dir) {
    $file_list = scandir($dir . '/');
    foreach ($file_list as $f) {
        if (is_file($dir . '/' . $f)) {
            $file_info = pathinfo($f);
            if ($file_info["extension"] == "php") {
                include_once($dir . '/' . $f);
            }
        }
    }
}
/* include api modules */

if (!function_exists('wpeae_get_api_list')) {

    function wpeae_get_api_list() {
        global $WPEAE_GLOBAL_API_LIST;
        return $WPEAE_GLOBAL_API_LIST;
    }

}

if (!function_exists('wpeae_get_api')) {

    function wpeae_get_api($type) {
        foreach (wpeae_get_api_list() as /* @var $api WPEAE_AbstractConfigurator */ $api) {
            if ($api->get_type() == $type) {
                return $api;
            }
        }
        return false;
    }

}

if (!function_exists('wpeae_get_api_path')) {

    function wpeae_get_api_path($api) {
        if ($api instanceof WPEAE_AbstractConfigurator) {
            return WPEAE_ROOT_PATH . 'includes/api/' . $api->get_type() . '/';
        }
        return "";
    }

}

if (!function_exists('wpeae_get_api_url')) {

    function wpeae_get_api_url($api) {
        if ($api instanceof WPEAE_AbstractConfigurator) {
            return WPEAE_ROOT_URL . 'includes/api/' . $api->get_type() . '/';
        }
        return false;
    }

}

if (!function_exists('wpeae_api_enqueue_style')) {
    function wpeae_api_enqueue_style($api) {
        foreach (glob(wpeae_get_api_path($api) . 'styles/', GLOB_ONLYDIR) as $dir) {
            $file_list = scandir($dir . '/');
            foreach ($file_list as $f) {
                if (is_file($dir . '/' . $f)) {
                    $file_info = pathinfo($f);
                    if ($file_info["extension"] == "css") {
                        wp_enqueue_style('wpeae-' . $api->get_type() . '-' . $file_info["filename"], wpeae_get_api_url($api) . 'styles/' . $file_info["basename"], array(), $api->get_config_value("version"));
                    }
                }
            }
        }
    }
}

if (!function_exists('wpeae_get_loader')) {

    function wpeae_get_loader($type) {
        $api_list = wpeae_get_api_list();
        foreach ($api_list as $api) {
            if ($api->get_type() === $type && class_exists($api->get_config_value("loader_class"))) {
                $class_name = $api->get_config_value("loader_class");
                return new $class_name($api);
            }
        }
        return false;
    }

}

if (!function_exists('wpeae_get_account')) {

    function wpeae_get_account($type) {
        $api_list = wpeae_get_api_list();
        foreach ($api_list as $api) {
            if ($api->get_type() === $type && class_exists($api->get_config_value("account_class"))) {
                $class_name = $api->get_config_value("account_class");
                return new $class_name($api);
            }
        }
        return false;
    }

}

if (!function_exists('wpeae_error_handler')) {

    function wpeae_error_handler($errno, $errstr, $errfile, $errline) {
        if (!(error_reporting() & $errno)) {
            return;
        }

        switch ($errno) {
            case E_USER_ERROR:
                $mess = "<b>ERROR</b> [$errno] $errstr<br />\n Fatal error on line $errline in file $errfile, PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                throw new Exception($mess);
            case E_USER_WARNING:
                $mess = "<b>My WARNING</b> [$errno] $errstr<br />\n";
                throw new Exception($mess);

            case E_USER_NOTICE:
                $mess = "<b>My NOTICE</b> [$errno] $errstr<br />\n";
                throw new Exception($mess);

            default:
                $mess = "Unknown error[$errno] on line $errline in file $errfile: $errstr<br />\n";
                throw new Exception($mess);
        }

        /* Don't execute PHP internal error handler */
        return true;
    }

}

if (!function_exists('WPEAE_DEBUG_TRACE')) {

    function WPEAE_DEBUG_TRACE($var, $label = "") {
        if (WP_DEBUG) {
            echo "DEBUG_TRACE " . $label . ": ";
            if (is_array($var) || is_object($var)) {
                echo "<pre>";
                print_r($var);
                echo "</pre><br/>";
            } else {
                echo $var . "<br/>";
            }
        }
    }

}

if (!function_exists('wpeae_log')) {

    function wpeae_log($message) {
        if (WP_DEBUG === true) {
            if (is_array($message) || is_object($message)) {
                error_log(print_r($message, true));
            } else {
                error_log($message);
            }
        }
    }

}
