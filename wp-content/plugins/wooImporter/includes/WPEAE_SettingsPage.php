<?php

/**
 * Description of WPEAE_SettingsPage
 *
 * @author Geometrix
 */
if (!class_exists('WPEAE_SettingsPage')):

    class WPEAE_SettingsPage {

        public $account_forms = array();

        function render() {
            $api_list = wpeae_get_api_list();
            $api_accounts = array();
            foreach ($api_list as $api) {
                if ($api->is_instaled()) {
                    $api_accounts[] = wpeae_get_account($api->get_type());
                }
            }

            if (isset($_POST['setting_form'])) {
                foreach ($api_accounts as $api_account) {
                    $api_account->save($_POST);
                }

                update_option('wpeae_price_multiplier', isset($_POST['wpeae_price_multiplier']) ? $_POST['wpeae_price_multiplier'] : 1);
                update_option('wpeae_currency_conversion_factor', isset($_POST['wpeae_currency_conversion_factor']) ? $_POST['wpeae_currency_conversion_factor'] : 1);
                update_option('wpeae_per_page', isset($_POST['wpeae_per_page']) ? $_POST['wpeae_per_page'] : 1);
                update_option('wpeae_default_type', isset($_POST['wpeae_default_type']) ? $_POST['wpeae_default_type'] : 1);
                update_option('wpeae_default_shipment_charges', isset($_POST['wpeae_default_shipment_charges']) ? $_POST['wpeae_default_shipment_charges'] : 1);
                update_option('wpeae_use_source_shipment', isset($_POST['wpeae_use_source_shipment']));
                update_option('wpeae_import_attributes', isset($_POST['wpeae_import_attributes']));
                update_option('wpeae_use_affiliate_url', isset($_POST['wpeae_use_affiliate_url']));

                if (isset($_POST['wpeae_default_status'])) {
                    update_option('wpeae_default_status', $_POST['wpeae_default_status']);
                }

                update_option('wpeae_price_auto_update', isset($_POST['wpeae_price_auto_update']));
                if (isset($_POST['wpeae_price_auto_update_period'])) {
                    update_option('wpeae_price_auto_update_period', $_POST['wpeae_price_auto_update_period']);
                }


                $price_auto_update = get_option('wpeae_price_auto_update', false);
                if ($price_auto_update) {
                    wp_schedule_event(time(), get_option('wpeae_price_auto_update_period', 'daily'), 'wpeae_update_price_event');
                } else {
                    wp_clear_scheduled_hook('wpeae_update_price_event');
                }
            } else {
                foreach ($api_accounts as $api_account) {
                    $api_account->save_default($_GET);
                    $api_account->load();
                }
            }

            $this->account_forms = array();
            foreach ($api_accounts as $api_account) {
                $this->account_forms[] = $api_account->get_form();
            }

            include(WPEAE_ROOT_PATH . '/view/settings.php' );
        }

    }

    

endif;
