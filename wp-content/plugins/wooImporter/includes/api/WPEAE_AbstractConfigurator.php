<?php

/**
 * Description of WPEAE_AbstractConfigurator
 *
 * @author Geometrix
 */
if (!class_exists('WPEAE_AbstractConfigurator')):

    abstract class WPEAE_AbstractConfigurator {

        private $filter_config = array();

        public function __construct() {
            wpeae_add_api($this);
            $this->check_api_configure();

            $this->init_filters();
            $this->configure_filters();
        }

        // should return config array!!!
        abstract public function get_config();

        public function get_type() {
            return $this->get_config_value("type");
        }

        public function get_config_value($key) {
            $config = $this->get_config();
            return isset($config[$key]) ? $config[$key] : false;
        }

        public function is_instaled() {
            $config = $this->get_config();
            return (is_array($config) && count($config) && isset($config['instaled']) && $config['instaled']) ? true : false;
        }

        public function print_promo_page() {
            include_once WPEAE_ROOT_PATH . 'view/default_promo_page.php';
        }

        public function install() {
            
        }

        public function uninstall() {
            
        }
        
        // configure common filters
        protected function init_filters() {
            wpeae_add_api_filter($this, "productId", "productId", 10, array("type" => "edit",
                "label" => "ProductId",
                "dop_row" => "OR configure search filter",
                "placeholder" => "Please enter your productId"));
            wpeae_add_api_filter($this, "query", "query", 20, array("type" => "edit",
                "label" => "Keywords",
                "placeholder" => "Please enter your Keywords"));
            wpeae_add_api_filter($this, "price", array("min_price", "max_price"), 30, array("type" => "edit",
                "label" => "Price",
                "min_price" => array("label" => "from $", "default" => "0.00"),
                "max_price" => array("label" => " to $", "default" => "0.00")));
        }

        // configure custom api filters
        protected function configure_filters() {
            
        }

        public function add_filter($id, $name, $order = 1000, $config = array()) {
            $this->filter_config[$id] = array('id' => $id, 'name' => $name, 'config' => $config, 'order' => $order);
        }

        public function remove_filter($id) {
            unset($this->filter_config[$id]);
        }

        public function get_filters() {
            $result = array();
            foreach ($this->filter_config as $id => $filter) {
                $result[$id] = $filter;
                if (isset($filter['config']['data_source']) && $filter['config']['data_source']) {
                    if (is_array($filter['config']['data_source'])) {
                        $result[$id]['config']['data_source'] = $filter['config']['data_source'][0]->$filter['config']['data_source'][1]();
                    } else {
                        $result[$id]['config']['data_source'] = $filter['config']['data_source']();
                    }
                }
            }
            if (!function_exists('WPEAE_AbstractConfigurator_cmp')) {

                function WPEAE_AbstractConfigurator_cmp($a, $b) {
                    if ($a['order'] == $b['order']) {
                        return 0;
                    }
                    return ($a['order'] < $b['order']) ? -1 : 1;
                }

            }
            uasort($result, 'WPEAE_AbstractConfigurator_cmp');

            return $result;
        }

        private function check_api_configure() {
            //trigger_error('test', E_USER_WARNING);
            $config = $this->get_config();
            if (!is_array($config)) {
                throw new Exception('WPEAE Error: ' . get_called_class() . ' uncorect API configure! get_config() must return array');
            } else if (!isset($config['type']) || !$config['type']) {
                throw new Exception('WPEAE Error: ' . get_called_class() . ' uncorect API configure! Config array must have not empty "type"');
            } else if ($this->is_instaled() && !isset($config['account_class'])) {
                throw new Exception('WPEAE Error: ' . get_called_class() . ' uncorect API configure! Config array must have correct "account_class"');
            } else if ($this->is_instaled() && !isset($config['loader_class'])) {
                throw new Exception('WPEAE Error: ' . get_called_class() . ' uncorect API configure! Config array must have correct "loader_class"');
            }
        }

    }
    
endif;