<?php

/**
 * Description of WPEAE_EBayConfigurator
 *
 * @author User
 */
if (!defined('WPEAE_TABLE_EBAY_SITES')) {
    define('WPEAE_TABLE_EBAY_SITES', 'wpeae_ebay_sites');
}

class WPEAE_EBayConfigurator extends WPEAE_AbstractConfigurator {

    public function get_config() {
        return array(
            "version" => "1.0",
            "instaled" => false,
            "type" => "ebay",
            "menu_title" => "Ebay",
            "dashboard_title" => "Ebay",
            
            "promo_title" => 'Ebay & Aliexpress WooCommerce Importer',
            "promo_text" => '<p>It’s a plugin that used to import products from Ebay and Aliexpress to your Wordpress WooCommerce site.</p><p>The plugin is helpful to create a store with specific Ebay & Aliexpress products and use affiliate URLs.</p>',
            "promo_link" => 'http://codecanyon.net/item/ebay-aliexpress-woocommerce-importer/13388576'
        );
    }
}

new WPEAE_EBayConfigurator();
