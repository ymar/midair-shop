<?php

/**
 * Description of WPEAE_WalmartConfigurator
 *
 * @author User
 */
class WPEAE_WalmartConfigurator extends WPEAE_AbstractConfigurator {

    public function get_config() {
        return array(
            "version" => "1.0",
            "instaled" => false,
            "type" => "walmart",
            "menu_title" => "Walmart",
            "dashboard_title" => "Walmart",
            
            "promo_title" => 'Walmart WooCommerce Importer',
            "promo_text" => '<p>It’s a plugin that used to import products from Walmart to your Wordpress WooCommerce site.</p><p>The plugin is helpful to create a store with specific Walmart products and use affiliate URLs.</p>',
            "promo_link" => 'http://codecanyon.net/item/walmart-woocommerce-importer/15279170'
        );
    }
}

new WPEAE_WalmartConfigurator();
