<?php

/**
 * Description of WPEAE_AbstractLoader
 *
 * @author Geometrix
 */
if (!class_exists('WPEAE_AbstractLoader')):

    abstract class WPEAE_AbstractLoader {

        public $account;
        public $api;

        public function __construct($api) {
            $this->api = $api;
            $this->account = wpeae_get_account($api->get_type());
        }

        abstract public function load_list($filter, $page = 1, $itemsPerPage = 25);

        abstract public function load_detail(&$goods, $params = array());

        abstract public function get_detail($productId, $params = array());

        abstract public function check_availability($goods);

        public function has_account() {
            return (isset($this->account) && $this->account->is_load());
        }

    }

    
endif;