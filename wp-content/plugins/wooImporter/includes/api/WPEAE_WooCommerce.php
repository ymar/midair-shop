<?php

/**
 * Description of WPEAE_WooCommerce
 *
 * @author Geometrix
 */
if (!class_exists('WPEAE_WooCommerce')) {

    class WPEAE_WooCommerce {

        public static function add_post($goods) {
            set_time_limit(500);
            $result = array("state" => "ok", "message" => "");
            global $wpdb;

            $product_status = get_option('wpeae_default_status', 'publish');

            $post = array(
                'post_title' => $goods->get_prop('title'),
                'post_content' => $goods->get_prop('description'),
                'post_status' => $product_status,
                'post_excerpt' => $goods->get_prop('title'),
                'post_name' => $goods->get_prop('title'),
                'post_type' => "product"
            );



            $product_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key='external_id' AND meta_value='%s' LIMIT 1", $goods->getId()));
            if (!$product_id) {
                $post_id = wp_insert_post($post);
            } else {
                $post_id = $product_id;
            }

            $categories = WPEAE_WooCommerce::build_categories($goods);
            $product_type = get_option('wpeae_default_type', 'simple');

            wp_set_object_terms($post_id, $categories, 'product_cat');
            wp_set_object_terms($post_id, $product_type, 'product_type');
            update_post_meta($post_id, '_stock_status', 'instock');
            update_post_meta($post_id, '_sku', (string) $goods->external_id);
            update_post_meta($post_id, '_product_url', $goods->detail_url);

            update_post_meta($post_id, 'condition', $goods->condition);
            update_post_meta($post_id, 'import_type', $goods->type);
            update_post_meta($post_id, 'external_id', (string) $goods->getId());
            update_post_meta($post_id, 'seller_url', $goods->seller_url);

            $quantity = 1;

            update_post_meta($post_id, '_manage_stock', 'yes');
            update_post_meta($post_id, '_stock', $quantity);
            update_post_meta($post_id, '_visibility', 'visible');
            update_post_meta($post_id, '_regular_price', $goods->user_price);
            update_post_meta($post_id, '_sale_price', $goods->user_price);
            update_post_meta($post_id, '_price', $goods->user_price);
            update_post_meta($post_id, 'ship_price', $goods->user_ship);

            $import_attributes = get_option('wpeae_import_attributes', false);
            if ($import_attributes) {
                $attribute = WPEAE_Goods::get_normalized_value($goods, "attribute");
                if ($attribute && ($arrAttribute = unserialize($attribute))) {
                    WPEAE_WooCommerce::set_attributes($post_id, $arrAttribute);
                }
            }

            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/media.php');

            //$home_url = get_home_url();

            $thumb_url = $goods->get_prop('image');
            if ($thumb_url) {
                $thumb_id = WPEAE_WooCommerce::image_attacher($thumb_url, $product_id);
                set_post_thumbnail($post_id, $thumb_id);
            }

            $images_url = $goods->getAllPhotos();

            $image_gallery_ids = '';
            foreach (array_slice($images_url, 1) as $image_url) {
                if ($thumb_url != $image_url) {
                    try {
                        $image_gallery_ids .= WPEAE_WooCommerce::image_attacher($image_url, $post_id) . ',';
                    } catch (Exception $e) {
                        $result['state'] = "warn";
                        $result['message'] = "\nimg_warn: $image_url";
                    }
                }
            }
            update_post_meta($post_id, '_product_image_gallery', $image_gallery_ids);

            return $result;
        }

        public static function update_post($goods) {
            $result = array("state" => "ok", "message" => "");

            return $result;
        }

        public static function build_categories($goods) {
            return ($goods->link_category_id) ? IntVal($goods->link_category_id) : $goods->category_name;
        }

        public static function image_attacher($image_url, $post_id) {
            $image = WPEAE_WooCommerce::download_url($image_url);
            if ($image) {
                $file_array = array(
                    'name' => basename($image),
                    'size' => filesize($image),
                    'tmp_name' => $image
                );
                return media_handle_sideload($file_array, $post_id);
            } else {
                return false;
            }
        }

        public static function download_url($url) {
            $wp_upload_dir = wp_upload_dir();
            $parsed_url = parse_url($url);
            $pathinfo = pathinfo($parsed_url['path']);

            //$dest_filename = wp_unique_filename($wp_upload_dir['path'], $pathinfo['basename']);
            $dest_filename = wp_unique_filename($wp_upload_dir['path'], mt_rand() . "." . $pathinfo['extension']);

            $dest_path = $wp_upload_dir['path'] . '/' . $dest_filename;

            $response = wp_remote_get($url);
            if (is_wp_error($response)) {
                return false;
            } elseif (!in_array($response['response']['code'], array(404, 403))) {
                file_put_contents($dest_path, $response['body']);
            }

            if (!file_exists($dest_path)) {
                return false;
            } else {
                return $dest_path;
            }
        }

        public static function set_attributes($post_id, $attributes) {
            $name = array_column($attributes, 'name');
            $count = array_count_values($name);
            $duplicate = array_unique(array_diff_assoc($name, array_unique($name)));
            $product_attributes = '';

            foreach ($attributes as $name => $value) {
                if (isset($duplicate[$name + 1])) {
                    $val = array();
                    for ($i = 0; $i < $count[$value['name']]; $i++) {
                        $val[] = $attributes[$name + $i]['value'];
                    }
                    $product_attributes[str_replace(' ', '-', $value['name'])] = array(
                        'name' => $value['name'],
                        'value' => implode(', ', $val),
                        'position' => 0,
                        'is_visible' => 1,
                        'is_variation' => 0,
                        'is_taxonomy' => 0
                    );
                } elseif (!array_search($value['name'], $duplicate)) {
                    $product_attributes[str_replace(' ', '-', $value['name'])] = array(
                        'name' => $value['name'],
                        'value' => $value['value'],
                        'position' => 0,
                        'is_visible' => 1,
                        'is_variation' => 0,
                        'is_taxonomy' => 0
                    );
                }
            }
            update_post_meta($post_id, '_product_attributes', $product_attributes);
        }

    }

}

if (!function_exists('array_column')) {

    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null) {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
            return null;
        }

        if (!is_int($params[1]) && !is_float($params[1]) && !is_string($params[1]) && $params[1] !== null && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2]) && !is_int($params[2]) && !is_float($params[2]) && !is_string($params[2]) && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {

            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }
        }

        return $resultArray;
    }

}