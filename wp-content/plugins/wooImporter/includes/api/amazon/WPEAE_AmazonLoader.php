<?php

/**
 * Description of WPEAE_AmazonLoader
 *
 * @author Geometrix
 */
if (!class_exists('WPEAE_AmazonLoader')):

    class WPEAE_AmazonLoader extends WPEAE_AbstractLoader {
    
        public function load_list($filter, $page = 1, $per_page = 10) {
            $result = array("total" => 0, "per_page" => $per_page, "items" => array(), "error" => "");
            if ((isset($filter['productId']) && !empty($filter['productId'])) || (isset($filter['query']) && !empty($filter['query'])) || (isset($filter['category_id']) && $filter['category_id'] != 0)) {
                $single_product_id = (isset($filter['productId']) && $filter['productId']) ? $filter['productId'] : "";
                
                $query = (isset($filter['query'])) ? utf8_encode($filter['query']) : "";
                
                $site = isset($filter['sitecode']) ? $filter['sitecode'] : "com";
                
                $category_id = (isset($filter['category_id']) && $filter['category_id']) ? $filter['category_id'] : "";
                $link_category_id = (isset($filter['link_category_id']) && IntVal($filter['link_category_id'])) ? IntVal($filter['link_category_id']) : 0;

                $priceFrom = (isset($_GET['min_price']) && !empty($_GET['min_price']) && floatval($filter['min_price']) > 0.009) ? sprintf("%01.2f", floatval($_GET['min_price'])) : false;
                $priceTo = (isset($_GET['max_price']) && !empty($_GET['max_price']) && floatval($filter['max_price']) > 0.009) ? sprintf("%01.2f", floatval($_GET['max_price'])) : false;

                $condition = (isset($filter['condition']) && $filter['condition']) ? $filter['condition'] : "New";
                // <---------------------------

                if ($single_product_id) {
                    $params = array(
                        "Operation" => "ItemLookup",
                        "ItemId" => $single_product_id,
                        "IdType" => "ASIN",
                        "ResponseGroup" => "Images,ItemAttributes,Large,OfferFull,Offers,OfferSummary"
                    );
                } else {
                    $params = array(
                        "Operation" => "ItemSearch",
                        "SearchIndex" => "All",
                        "ItemPage" => $page,
                        "Keywords" => $query,
                        "ResponseGroup" => "Images,ItemAttributes,Large,OfferFull,OfferListings,Offers,OfferSummary",
                            //"Sort" => "price"
                    );
                    
                    if($priceFrom){
                        $params['MinimumPrice'] = intval(floatval($priceFrom)*100);
                    }
                    if($priceTo){
                        $params['MaximumPrice'] = intval(floatval($priceTo)*100);
                    }
                    if($category_id){
                        $params['SearchIndex'] = $category_id;
                    }
                    if($condition){
                        $params['Condition'] = $condition;
                    }
                    if($condition!="New"){
                        $params['Availability'] = "Available";
                    }
                }
                //print_r($params);
                $response = $this->send_amazon_request($site,$params);

                if (isset($response['error'])) {
                    $result["error"] = $response['error'];
                } else {

                    $items = $response['Items']['Item'];

                    if ($single_product_id) {
                        $items = array($items);
                    }

                    $price_multiplier = floatval(get_option('wpeae_price_multiplier', 1));
                    $currency_conversion_factor = floatval(get_option('wpeae_currency_conversion_factor', 1));
                    foreach ($items as $item) {
                        //echo "<pre>";print_r($item);echo "</pre>";

                        $goods = $this->parse_amazon_item($item,array('condition'=>$condition));

                        $goods->save("API");

                        if (strlen(trim((string) $goods->user_price)) == 0) {
                            $goods->save_field("user_price", sprintf("%01.2f", floatval($goods->price) * $price_multiplier * $currency_conversion_factor));
                        }

                        if (strlen(trim((string) $goods->user_ship)) == 0) {
                            if (get_option('wpeae_use_source_shipment', false)) {
                                $goods->save_field("user_ship", $goods->ship);
                            } else {
                                $shipment_charges = get_option('wpeae_default_shipment_charges', '0.00');
                                $shipment_charges = $shipment_charges ? $shipment_charges : "0.00";
                                $goods->save_field("user_ship", $shipment_charges);
                            }
                        }

                        if (strlen(trim((string) $goods->user_image)) == 0) {
                            $goods->save_field("user_image", $goods->image);
                        }

                        $result["items"][] = $goods;
                    }
                    
                    $result["total"] = IntVal($response['Items']['TotalResults']);
                    
                }
            } else {
                $result["error"] = 'Please enter some search keywords or select item from category list!';
            }

            return $result;
        }

        public function load_detail(/* @var $goods WPEAE_Goods */ &$goods, $params = array()) {
            return array("state" => "ok", "message" => "", "goods" => $goods);
        }

        public function get_detail($productId, $params = array()) {
            $prms = array("Operation" => "ItemLookup", "ItemId" => $productId, "IdType" => "ASIN", "ResponseGroup" => "Images,ItemAttributes,Large,OfferFull,Offers,OfferSummary");
            $response = $this->send_amazon_request("com", $prms);

            if (isset($response['error'])) {
                return array('state' => 'error', 'message' => $response['error']);
            } else {
                $item = $response['Items']['Item'];

                if ($item) {
                    $goods = $this->parse_amazon_item($item);

                    $price_multiplier = floatval(get_option('wpeae_price_multiplier', 1));
                    $currency_conversion_factor = floatval(get_option('wpeae_currency_conversion_factor', 1));
                    $goods->user_price = floatval($goods->price) * $price_multiplier * $currency_conversion_factor;
                    return array("state" => "ok", "message" => "", "goods" => $goods);
                }
            }
        }

        public function check_availability(/* @var $goods WPEAE_Goods */ $goods) {
            return true;
        }

        private function parse_amazon_item($item, $params = array()) {
            $goods = new WPEAE_Goods();
            $goods->type = "amazon";
            $goods->external_id = $item["ASIN"];
            $goods->load();

            $goods->link_category_id = $link_category_id;

            $goods->image = (isset($item["LargeImage"]["URL"])) ? $item["LargeImage"]["URL"] : WPEAE_NO_IMAGE_URL;

            $goods->detail_url = $item["DetailPageURL"];

            $goods->title = $item["ItemAttributes"]["Title"];

            $goods->subtitle = "#notuse#";
            $goods->validTime = "#notuse#";
            $goods->keywords = "#notuse#";

            $goods->category_id = 0;
            $goods->category_name = $item["BrowseNodes"]["BrowseNode"]["Name"];

            $goods->description = "";
            foreach ($item['ItemAttributes'] as $attr => $value) {
                if ($attr == "Feature") {
                    $goods->description .= '<div class="feature"><span>Feature:</span>';
                    $goods->description .= '<ul>';
                    $value = is_array($value)?$value:array($value);
                    foreach ($value as $v) {
                        $goods->description .= "<li>".$v . "</li>";
                    }
                    $goods->description .= '</ul>';
                    $goods->description .= '</div>';
                }
            }
            if(isset($item['EditorialReviews']['EditorialReview'])){
                if(isset($item['EditorialReviews']['EditorialReview'][0])){
                    foreach($item['EditorialReviews']['EditorialReview'] as $dd){
                        if($dd['Source'] == 'Product Description'){
                            $goods->description .= '<div class="product_description">'.$dd['Content'].'</div>';
                        }
                    }
                }else if(isset($item['EditorialReviews']['EditorialReview']['Content'])){
                    $goods->description .= '<div class="product_description">'.$item['EditorialReviews']['EditorialReview']['Content'].'</div>';
                }
            }

            $attrs = array();
            $attr_exclude = array("EANList", "Feature", "Label", "PackageDimensions", "PackageQuantity", "ProductGroup", "ProductTypeName", "UPCList", "Title");
            foreach ($item['ItemAttributes'] as $attr => $value) {
                if (!in_array($attr, $attr_exclude) && !is_array($value)) {
                    $attrs[] = array("name" => $attr, "value" => $value);
                }
            }
            $goods->attribute = ($attrs) ? serialize($attrs) : "#empty#";

            $tmp_p = "";
            if(isset($item['ImageSets'])){
                foreach ($item['ImageSets']['ImageSet'] as $img) {
                    if ($img["@attributes"]["Category"] == "variant") {
                        $tmp_p .= ($tmp_p ? "," : "") . $img["LargeImage"]["URL"];
                    }
                }
            }
            $goods->photos = $tmp_p;


            $goods->ship_to_locations = "#notuse#";

            $tmp_condition = "";
            $tmp_seller = "";
            $tmp_curr = "";
            $tmp_price = 0;
            if(isset($item['Offers']['Offer'])){
                $tmp_offers = (intval($item['Offers']['TotalOffers']) == 1) ? array($item['Offers']['Offer']) : $item['Offers']['Offer'];
                foreach ($tmp_offers as $offer) {
                    if (!$tmp_curr || $tmp_price > (floatval($offer['OfferListing']['Price']['Amount']) / 100)) {
                        $tmp_curr = $offer['OfferListing']['Price']['CurrencyCode'];
                        $tmp_price = floatval($offer['OfferListing']['Price']['Amount']) / 100;
                        $tmp_condition = $offer['OfferAttributes']['Condition'];
                        $tmp_seller = $offer['Merchant']['Name'];
                    }
                }
            }else{
                if(isset($params['condition'])){
                    if(intval($item['OfferSummary']['Total'.$params['condition']])>0){
                        $tmp_condition = $params['condition'];
                        $tmp_curr = $offer['Lowest'.$params['condition'].'Price']['CurrencyCode'];
                        $tmp_price = floatval($offer['Lowest'.$params['condition'].'Price']['Amount']) / 100;
                    }
                }
                
                if(!$tmp_condition){
                    if(intval($item['OfferSummary']['TotalNew'])>0){
                        $tmp_condition = "New";
                        $tmp_curr = $offer['LowestNewPrice']['CurrencyCode'];
                        $tmp_price = floatval($offer['LowestNewPrice']['Amount']) / 100;
                    }else if(intval($item['OfferSummary']['TotalUsed'])>0){
                        $tmp_condition = "Used";
                        $tmp_curr = $offer['LowestUsedPrice']['CurrencyCode'];
                        $tmp_price = floatval($offer['LowestUsedPrice']['Amount']) / 100;
                    }else if(intval($item['OfferSummary']['TotalCollectible'])>0){
                        $tmp_condition = "Collectible";
                        $tmp_curr = $offer['LowestCollectiblePrice']['CurrencyCode'];
                        $tmp_price = floatval($offer['LowestCollectiblePrice']['Amount']) / 100;
                    }else if(intval($item['OfferSummary']['TotalRefurbished'])>0){
                        $tmp_condition = "Refurbished";
                        $tmp_curr = $offer['LowestRefurbishedPrice']['CurrencyCode'];
                        $tmp_price = floatval($offer['LowestRefurbishedPrice']['Amount']) / 100;
                    }
                }
            }
            

            $goods->condition = $tmp_condition;
            
            /*if ($tmp_seller) {
                $goods->seller_url = "http://www.amazon.com/seller/" . $tmp_seller;
            }else{
                $goods->seller_url = "#notuse#";
            }*/
            $goods->seller_url = "#notuse#";


            $goods->price = WPEAE_Goods::get_normalize_price($tmp_price);
            $goods->ship = WPEAE_Goods::get_normalize_price("0.00");
            $goods->total_price = $tmp_price;

            $goods->curr = $tmp_curr;

            return $goods;
        }

        private function send_amazon_request($site_id, $prms = array()) {
            $params = is_array($prms) ? $prms : array();

            // The region you are interested in
            $endpoint = "webservices.amazon.".$site_id;
            $uri = "/onca/xml";

            $aws_associate_tag = $this->account->associate_tag;
            $aws_access_key_id = $this->account->access_key_id;
            $aws_secret_key = $this->account->secret_access_key;

            if (!isset($params['AWSAccessKeyId'])) {
                $params['AWSAccessKeyId'] = $aws_access_key_id;
            }
            if (!isset($params['AssociateTag'])) {
                $params['AssociateTag'] = $aws_associate_tag;
            }
            if (!isset($params['Service'])) {
                $params['Service'] = "AWSECommerceService";
            }

            // Set current timestamp if not set
            if (!isset($params["Timestamp"])) {
                $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
            }

            // Sort the parameters by key
            ksort($params);

            $pairs = array();

            foreach ($params as $key => $value) {
                array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
            }

            // Generate the canonical query
            $canonical_query_string = join("&", $pairs);

            // Generate the string to be signed
            $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;

            // Generate the signature required by the Product Advertising API
            $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

            // Generate the signed URL
            $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);

            //echo "Signed URL: \"" . $request_url . "\"<br/>";

            $response = wp_remote_get($request_url);
            //echo "<pre>";print_r($response);echo "</pre>";

            if (is_wp_error($response)) {
                return array("error" => "Amazon not response!");
            } else {
                if (wp_remote_retrieve_response_code($response) != '200') {
                    return array("error" => "[" . wp_remote_retrieve_response_code($response) . "] " . wp_remote_retrieve_response_message($response));
                } else {
                    $body = wp_remote_retrieve_body($response);
                    //echo "<pre>";print_r($body);echo "</pre>";
                    $response = simplexml_load_string($body);

                    $response_json = json_encode($response);
                    $response = json_decode($response_json, TRUE);

                    if ($response['Items']['Request']['IsValid'] == 'True') {
                        return $response;
                    } else {
                        return array("error" => $response_xml->Items->Request->Errors->Error->Code . "; " . $response_xml->Items->Request->Errors->Error->Message);
                    }
                }
            }
        }

    }

    

    

    

    

    

    

    

    

    

    

endif;