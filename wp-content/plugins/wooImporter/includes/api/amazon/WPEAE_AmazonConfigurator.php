<?php

/**
 * Description of WPEAE_AmazonConfigurator
 *
 * @author Geometrix
 */
class WPEAE_AmazonConfigurator extends WPEAE_AbstractConfigurator {

    public function get_config() {
        return array(
            "version" => "1.1",
            "instaled" => true,
            "type" => "amazon",
            "menu_title" => "Amazon",
            "dashboard_title" => "Amazon",
            
            "default_item_per_page" => 10,
            "account_class" => "WPEAE_AmazonAccount",
            "loader_class" => "WPEAE_AmazonLoader",
            "exclude_filters" => array("shipment", "commisionRate", "volume", "feedback_score", "store"),
            "exclude_columns" => array("ship", "validTime", "commisionRate", "ship_to_locations"),
            "sort_columns" => array(),
            
            "promo_title" => 'Amazon WooCommerce Importer',
            "promo_text" => '<p>It’s a plugin that used to import products from Amazon to your Wordpress WooCommerce site.</p><p>The plugin is helpful to create a store with specific Amazon products and use affiliate URLs.</p>',
            "promo_link" => 'http://codecanyon.net/item/amazon-woocommerce-importer/15279150'
        );
    }

    protected function configure_filters() {

        wpeae_add_api_filter($this, "category_id", "category_id", 21, array("type" => "select",
            "label" => "Category",
            "class" => "category_list",
            "data_source" => array($this, 'get_categories')));

        wpeae_add_api_filter($this, "condition", "condition", 34, array("type" => "select",
            "label" => "Condition",
            "class" => "sitecode_list",
            "data_source" => array($this, 'get_condition_list')));

        wpeae_add_api_filter($this, "sitecode", "sitecode", 35, array("type" => "select",
            "label" => "Site",
            "class" => "sitecode_list",
            "data_source" => array($this, 'get_sites')));
    }

    protected function get_categories() {
        return array(
            array("id" => "", "name" => " - "),
            array("id" => "All", "name" => "All"),
            array("id" => "Appliances", "name" => "Appliances"),
            array("id" => "ArtsAndCrafts", "name" => "ArtsAndCrafts"),
            array("id" => "Automotive", "name" => "Automotive"),
            array("id" => "Baby", "name" => "Baby"),
            array("id" => "Beauty", "name" => "Beauty"),
            array("id" => "Blended", "name" => "Blended"),
            array("id" => "Books", "name" => "Books"),
            array("id" => "Collectibles", "name" => "Collectibles"),
            array("id" => "Electronics", "name" => "Electronics"),
            array("id" => "Fashion", "name" => "Fashion"),
            array("id" => "FashionBaby", "name" => "FashionBaby"),
            array("id" => "FashionBoys", "name" => "FashionBoys"),
            array("id" => "FashionGirls", "name" => "FashionGirls"),
            array("id" => "FashionMen", "name" => "FashionMen"),
            array("id" => "FashionWomen", "name" => "FashionWomen"),
            array("id" => "GiftCards", "name" => "GiftCards"),
            array("id" => "Grocery", "name" => "Grocery"),
            array("id" => "HealthPersonalCare", "name" => "HealthPersonalCare"),
            array("id" => "HomeGarden", "name" => "HomeGarden"),
            array("id" => "Industrial", "name" => "Industrial"),
            array("id" => "KindleStore", "name" => "KindleStore"),
            array("id" => "LawnAndGarden", "name" => "LawnAndGarden"),
            array("id" => "Luggage", "name" => "Luggage"),
            array("id" => "MP3Downloads", "name" => "MP3Downloads"),
            array("id" => "Magazines", "name" => "Magazines"),
            array("id" => "Merchants", "name" => "Merchants"),
            array("id" => "MobileApps", "name" => "MobileApps"),
            array("id" => "Movies", "name" => "Movies"),
            array("id" => "Music", "name" => "Music"),
            array("id" => "MusicalInstruments", "name" => "MusicalInstruments"),
            array("id" => "OfficeProducts", "name" => "OfficeProducts"),
            array("id" => "PCHardware", "name" => "PCHardware"),
            array("id" => "PetSupplies", "name" => "PetSupplies"),
            array("id" => "Software", "name" => "Software"),
            array("id" => "SportingGoods", "name" => "SportingGoods"),
            array("id" => "Tools", "name" => "Tools"),
            array("id" => "Toys", "name" => "Toys"),
            array("id" => "UnboxVideo", "name" => "UnboxVideo"),
            array("id" => "VideoGames", "name" => "VideoGames"),
            array("id" => "Wine", "name" => "Wine"),
            array("id" => "Wireless", "name" => "Wireless"),
        );
    }

    protected function get_sites() {
        return array(
            array("id" => "com", "name" => "com"),
            array("id" => "de", "name" => "de"),
            array("id" => "co.uk", "name" => "co.uk"),
            array("id" => "ca", "name" => "ca"),
            array("id" => "fr", "name" => "fr"),
            array("id" => "co.jp", "name" => "co.jp"),
            array("id" => "it", "name" => "it"),
            array("id" => "cn", "name" => "cn"),
            array("id" => "es", "name" => "es")
        );
    }

    protected function get_condition_list() {
        return array(
            array("id" => "New", "name" => "New"),
            array("id" => "Used", "name" => "Used"),
            array("id" => "Collectible", "name" => "Collectible"),
            array("id" => "Refurbished", "name" => "Refurbished"),
            array("id" => "All", "name" => "All"),
        );
    }

}

new WPEAE_AmazonConfigurator();
