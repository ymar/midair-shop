<?php

/**
 * Description of WPEAE_Ajax
 *
 * @author Geometrix
 */
if (!class_exists('WPEAE_Ajax')):

    class WPEAE_Ajax {

        function __construct() {
            add_action('wp_ajax_wpeae_edit_goods', array($this, 'edit_goods'));
            add_action('wp_ajax_wpeae_select_image', array($this, 'select_image'));
            add_action('wp_ajax_wpeae_load_details', array($this, 'load_details'));
            add_action('wp_ajax_wpeae_import_goods', array($this, 'import_goods'));
            add_action('wp_ajax_wpeae_schedule_import_goods', array($this, 'schedule_import_goods'));
            add_action('wp_ajax_wpeae_upload_image', array($this, 'upload_image'));

            add_action('wp_ajax_wpeae_description_editor', array($this, 'description_editor'));
        }

        function description_editor() {
            $goods = new WPEAE_Goods(isset($_POST['id']) ? $_POST['id'] : "");
            $goods->load();

            if ($goods->photos == "#needload#") {
                echo '<h3><font style="color:red;">Description not load yet! Click "load more details"</font></h3>';
            } else {
                wp_editor($goods->get_prop("description"), $goods->external_id, array('media_buttons' => FALSE));
                echo '<input type="hidden" class="item_id" value="' . $goods->getId() . '"/>';
                echo '<input type="hidden" class="editor_id" value="' . $goods->external_id . '"/>';
                echo '<input type="button" class="save_description button" value="Save description"/>';

                _WP_Editors::enqueue_scripts();
                print_footer_scripts();
                _WP_Editors::editor_js();
            }

            wp_die();
        }

        function edit_goods() {
            $result = array("state" => "ok", "message" => "");
            try {
                set_error_handler("wpeae_error_handler");

                $goods = new WPEAE_Goods(isset($_POST['id']) ? $_POST['id'] : "");
                $goods->load();

                $field = (isset($_POST['field']) ? $_POST['field'] : false);
                $value = (isset($_POST['value']) ? $_POST['value'] : "");

                //if (get_magic_quotes_gpc()) {
                $value = stripslashes($value);
                //}

                if ($field && property_exists(get_class($goods), $field)) {
                    $goods->$field = $value;
                    $goods->save_field($field, $value);
                }

                restore_error_handler();
            } catch (Exception $e) {
                $result['state'] = 'error';
                $result['message'] = $e->getMessage();
            }

            echo json_encode($result);

            wp_die();
        }

        function select_image() {
            $result = array("state" => "ok", "message" => "");
            try {
                set_error_handler("wpeae_error_handler");

                $goods = new WPEAE_Goods(isset($_POST['id']) ? $_POST['id'] : "");
                if ($goods->load()) {
                    $goods->save_field('user_image', isset($_POST['image']) ? $_POST['image'] : "");
                }

                restore_error_handler();
            } catch (Exception $e) {
                $result['state'] = 'error';
                $result['message'] = $e->getMessage();
            }

            echo json_encode($result);

            wp_die();
        }

        function load_details() {
            $result = array("state" => "ok", "message" => "", "goods" => array(), "images_content" => "");
            try {
                set_error_handler("wpeae_error_handler");

                $goods = new WPEAE_Goods(isset($_POST['id']) ? $_POST['id'] : "");

                $edit_fields = isset($_POST['edit_fields']) ? $_POST['edit_fields'] : "";
                if ($edit_fields) {
                    $edit_fields = explode(",", $edit_fields);
                }

                $goods->load();

                $loader = wpeae_get_loader($goods->type);
                if ($loader) {
                    $res = $loader->load_detail($goods);

                    if ($res['state'] == "ok") {
                        $description_content = WPEAE_DashboardPage::put_description_edit($goods, true);
                        $goods->description = "#hidden#";
                        $result = array("state" => "ok", "goods" => WPEAE_Goods::get_normalized_object($goods, $edit_fields), "images_content" => WPEAE_DashboardPage::put_image_edit($goods, true), "description_content" => $description_content);
                    } else {
                        $result['state'] = $res['state'];
                        $result['message'] = $res['message'];
                    }
                }
                restore_error_handler();
            } catch (Exception $e) {
                $result['state'] = 'error';
                $result['message'] = $e->getMessage();
            }
            echo json_encode($result);
            wp_die();
        }

        function import_goods() {
            $result = array("state" => "ok", "message" => "");
            try {
                set_error_handler("wpeae_error_handler");

                $goods = new WPEAE_Goods(isset($_POST['id']) ? $_POST['id'] : "");

                if ($goods->load()) {
                    if ($goods->need_load_more_detail()) {
                        $loader = wpeae_get_loader($goods->type);
                        $result = $loader->load_detail($goods);
                    }
                    $goods->save_field("user_schedule_time", NULL);
                    if (!$goods->post_id && class_exists('WPEAE_WooCommerce')) {
                        $result = WPEAE_WooCommerce::add_post($goods);
                    }
                } else {
                    $result['state'] = 'error';
                    $result['message'] = "Product " . $_POST['id'] . " not find.";
                }
                restore_error_handler();
            } catch (Exception $e) {
                $result['state'] = 'error';
                $result['message'] = $e->getMessage();
            }

            echo json_encode($result);

            wp_die();
        }

        function schedule_import_goods() {
            $result = array("state" => "ok", "message" => "");
            try {
                set_error_handler("wpeae_error_handler");

                $time_str = isset($_POST['time']) ? $_POST['time'] : "";
                $time = $time_str ? date("Y-m-d H:i:s", strtotime($time_str)) : "";

                $goods = new WPEAE_Goods(isset($_POST['id']) ? $_POST['id'] : "");
                if ($goods->load() && $time) {
                    $result['message'] = $_POST['id'] . " loaded " . $time;
                    $result['time'] = date("m/d/Y H:i", strtotime($time));
                    $goods->save_field("user_schedule_time", $time);
                } else {
                    $result['message'] = $_POST['id'] . " not loaded " . $time;
                }
                restore_error_handler();
            } catch (Exception $e) {
                $result['state'] = 'error';
                $result['message'] = $e->getMessage();
            }

            echo json_encode($result);

            wp_die();
        }

        function upload_image() {
            $result = array("state" => "warning", "message" => "file not found");
            try {
                set_error_handler("wpeae_error_handler");

                $goods = new WPEAE_Goods(isset($_POST['upload_product_id']) ? $_POST['upload_product_id'] : "");

                if ($goods->load()) {
                    if (!function_exists('wp_handle_upload')) {
                        require_once( ABSPATH . 'wp-admin/includes/file.php' );
                    }

                    //$result["files"] = $_FILES;

                    if ($_FILES) {
                        foreach ($_FILES as $file => $array) {
                            if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) {
                                $result["state"] = "error";
                                $result["message"] = "upload error : " . $_FILES[$file]['error'];
                            }

                            $upload_overrides = array('test_form' => false);
                            $movefile = wp_handle_upload($array, $upload_overrides);

                            if ($movefile && !isset($movefile['error'])) {
                                $movefile["url"];
                                $goods->user_photos .= ($goods->user_photos ? "," : "") . $movefile["url"];
                                $goods->save_field("user_photos", $goods->user_photos);
                                $goods->save_field("user_image", $movefile["url"]);
                                $result["state"] = "ok";
                                $result["message"] = "";
                                $result["goods"] = $goods;
                                $result["images_content"] = WPEAE_DashboardPage::put_image_edit($goods, true);
                                $result["cur_image"] = $goods->get_prop('image');
                            } else {
                                $result["state"] = "error";
                                $result["message"] = "E1: " . $movefile['error'];
                            }
                        }
                    }
                }

                restore_error_handler();
            } catch (Exception $e) {
                $result['state'] = 'error';
                $result['message'] = $e->getMessage();
            }
            echo json_encode($result);
            wp_die();
        }

    }

    endif;

new WPEAE_Ajax();
