<?php

if (!function_exists('wpeae_schedule_proc')) {

    function wpeae_schedule_proc($show_trace = true) {
        set_error_handler("wpeae_error_handler");
        if ($show_trace) {
            echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): posted schedule products<br/>";
        }
        $list = WPEAE_Goods::load_list(1, 100, " AND NULLIF(NULLIF(user_schedule_time, '0000-00-00 00:00:00'), '') IS NOT NULL AND user_schedule_time < now()");

        if ($list["items"]) {
            foreach ($list["items"] as $goods) {
                try {
                    if ($show_trace) {
                        echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): check date {$goods->user_schedule_time}<br/>";
                    }

                    if ($show_trace) {
                        echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): posted...<br/>";
                    }

                    $loader = wpeae_get_loader($goods->type);

                    if ($loader) {
                        if ($goods->need_load_more_detail()) {
                            $result = $loader->load_detail($goods);
                        }

                        if (!$goods->post_id && class_exists('WPEAE_WooCommerce')) {
                            $result = WPEAE_WooCommerce::add_post($goods);
                        }

                        $goods->save_field("user_schedule_time", NULL);

                        if ($show_trace) {
                            echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): ok<br/>";
                        }
                    } else {
                        if ($show_trace) {
                            echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): loader not found<br/>";
                        }
                    }
                } catch (Exception $e) {
                    echo $e->getMessage() . "<br/>";
                }
            }
        } else {
            if ($show_trace) {
                echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): products to post not found<br/>";
            }
        }

        try {
            if ($show_trace) {
                echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): check availability<br/>";
            }
            $products = get_posts(array('post_type' => 'product'));
            foreach ($products as $post) {
                $product = new WC_Product($post->ID);
                $loader = false;


                $external_id = get_post_meta($post->ID, "external_id", true);

                $tmp_id = explode("#", $external_id);
                if ($tmp_id) {
                    $loader = wpeae_get_loader($tmp_id[0]);
                }

                if ($loader) {
                    $goods = new WPEAE_Goods($external_id);
                    if (!$loader->check_availability($goods)) {
                        if ($show_trace) {
                            echo "TARCE (" . date("Y-m-d H:i:s", time()) . "):move to trash {$post->ID}<br>";
                        }
                        wp_trash_post($post->ID);
                    } else {
                        if ($show_trace) {
                            echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): product {$post->ID} OK<br>";
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage() . "<br/>";
        }

        restore_error_handler();
    }

}
add_action('wpeae_schedule_post_event', 'wpeae_schedule_proc');


if (!function_exists('wpeae_update_price_proc')) {

    function wpeae_update_price_proc($show_trace = true) {
        if (!get_option('wpeae_price_auto_update', false)) {
            return;
        }

        set_error_handler("wpeae_error_handler");
        try {
            if ($show_trace) {
                echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): update price<br/>";
            }
            $products = get_posts(array('post_type' => 'product'));
            foreach ($products as $product) {
                $external_id = get_post_meta($product->ID, "external_id", true);
                if ($external_id) {
                    $goods = new WPEAE_Goods($external_id);
                    /* @var $loader WPEAE_AbstractLoader */
                    $loader = wpeae_get_loader($goods->type);

                    if ($loader) {
                        $result = $loader->get_detail($goods->external_id);

                        if ($result['state'] == "ok") {
                            $goods = $result['goods'];
                            update_post_meta($product->ID, '_price', $goods->user_price);
                            if ($show_trace) {
                                echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): update price for {$goods->getId()}: {$goods->user_price}<br>";
                            }
                        } else {
                            if ($show_trace) {
                                echo "TARCE (" . date("Y-m-d H:i:s", time()) . "): error while update price for {$product->ID}: {$result['message']}<br>";
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage() . "<br/>";
        }

        restore_error_handler();
    }

}
add_action('wpeae_update_price_event', 'wpeae_update_price_proc');
