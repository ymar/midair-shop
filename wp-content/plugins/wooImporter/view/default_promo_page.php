<div class="adm-wrap">
  <h3><?php echo $this->get_config_value("promo_title"); ?></h3>
  <div class="adm-text">
  <?php echo $this->get_config_value("promo_text"); ?>
  </div>
  <div class="adm-banner"></div> 
  <a href="<?php echo $this->get_config_value("promo_link"); ?>" class="adm-btn">Get it right now</a>
  <?php $plugin_data = get_plugin_data(WPEAE_FILE_FULLNAME);?>
  <p class="adm-sub-text">*This is the extension of the <?php echo $plugin_data['Name']; ?> plugins</p>
  <div class="adm-geometrix">
	<a alt="developer" href="http://gmetrixteam.com/"><img src="<?php echo WPEAE_ROOT_URL; ?>assets/img/geo.png"></a><br>
	<span>WordPress Plugins Development Team</span>
  </div>
</div>
