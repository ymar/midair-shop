<div>
	<h1><?php echo WPEAE_NAME;?> settings</h1>
	<form method="post">
		<input type="hidden" name="setting_form" value="1"/>

		<?php foreach ($this->account_forms as $af): ?>
			<?php foreach ($af["fields"] as $field): ?>
				<?php if ($field['type'] == "hidden"): ?>
					<input type="hidden" id="<?php echo $field["id"]; ?>" name="<?php echo $field["name"]; ?>" value="<?php echo $field["value"]; ?>"/>
				<?php endif; ?>
			<?php endforeach; ?>
			<h3><?php echo $af["title"]; ?></h3>
			<?php if ($af["use_default_account"]): ?>
				Using Default <a href="<?php echo ((isset($_GET['page'])) ? "?page=" . $_GET['page'] : ''); ?>&<?php echo $af["use_custom_account_param"] ?>=1">[Change]</a>
			<?php else: ?>
				Using Custom <a href="<?php echo ((isset($_GET['page'])) ? "?page=" . $_GET['page'] : ''); ?>&<?php echo $af["use_default_account_param"] ?>=1">[Change]</a>

				<table class="form-table">
					<?php foreach ($af["fields"] as $field): ?>
						<?php if ($field['type'] != "hidden"): ?>
							<tr valign="top">
								<th scope="row" class="titledesc"><label for="<?php echo $field["id"]; ?>"><?php echo $field["title"]; ?></label></th>
								<td class="forminp forminp-text"><input type="text" id="<?php echo $field["id"]; ?>" name="<?php echo $field["name"]; ?>" value="<?php echo $field["value"]; ?>"/></td>
							</tr>
						<?php endif; ?>
					<?php endforeach; ?>
				</table>
			<?php endif; ?>
		<?php endforeach; ?>

		<h3>Common setting</h3>
		<table class="form-table">
			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_price_multiplier">Price Multiplier</label></th>
				<td class="forminp forminp-text"><input type="text" id="wpeae_price_multiplier" name="wpeae_price_multiplier" value="<?php echo get_option('wpeae_price_multiplier'); ?>"/></td>
			</tr>
			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_currency_conversion_factor">Currency conversion factor</label></th>
				<td class="forminp forminp-text"><input type="text" id="wpeae_currency_conversion_factor" name="wpeae_currency_conversion_factor" value="<?php echo get_option('wpeae_currency_conversion_factor', '1'); ?>"/></td>
			</tr>

			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_per_page">Products per page</label></th>
				<td class="forminp forminp-text"><input type="text" id="wpeae_per_page" name="wpeae_per_page" value="<?php echo get_option('wpeae_per_page'); ?>"/></td>
			</tr>
			
			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_import_attributes">Use attributes</label></th>
				<td class="forminp forminp-text"><input type="checkbox" id="wpeae_import_attributes" name="wpeae_import_attributes" value="yes" <?php if (get_option('wpeae_import_attributes', false)): ?>checked<?php endif; ?>/></td>
			</tr>

			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_use_source_shipment">Use source shipment charges</label></th>
				<td class="forminp forminp-text"><input type="checkbox" id="wpeae_use_source_shipment" name="wpeae_use_source_shipment" value="yes" <?php if (get_option('wpeae_use_source_shipment', false)): ?>checked<?php endif; ?>/></td>
			</tr>

			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_default_shipment_charges">Shipment charges</label></th>
				<td class="forminp forminp-text"><input type="text" id="wpeae_default_shipment_charges" name="wpeae_default_shipment_charges" value="<?php echo get_option('wpeae_default_shipment_charges'); ?>" <?php if (get_option('wpeae_use_source_shipment', false)): ?>disabled<?php endif; ?>/></td>
			</tr>
			
			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_use_affiliate_url">Use affiliate product URLs</label></th>
				<td class="forminp forminp-text"><input type="checkbox" id="wpeae_use_affiliate_url" name="wpeae_use_affiliate_url" value="yes" <?php if (get_option('wpeae_use_affiliate_url', false)): ?>checked<?php endif; ?>/></td>
			</tr>

			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="wpeae_default_type">Default Product Type</label>
				</th>
				<td class="forminp forminp-select">
					<?php $cur_wpeae_default_type = get_option('wpeae_default_type', 'simple'); ?>
					<select name="wpeae_default_type" id="wpeae_default_type">
						<option value="simple" <?php if ($cur_wpeae_default_type == "simple"): ?>selected="selected"<?php endif; ?>>Simple Product</option>
						<option value="grouped" <?php if ($cur_wpeae_default_type == "grouped"): ?>selected="selected"<?php endif; ?>>Grouped Product</option>
						<option value="external" <?php if ($cur_wpeae_default_type == "external"): ?>selected="selected"<?php endif; ?>>External/Affiliate Product</option>
						<option value="variable" <?php if ($cur_wpeae_default_type == "variable"): ?>selected="selected"<?php endif; ?>>Variable Product</option>
					</select> 						</td>
			</tr>
			
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="wpeae_default_status">Default Product Status</label>
				</th>
				<td class="forminp forminp-select">
					<?php $cur_wpeae_default_status = get_option('wpeae_default_status', 'publish'); ?>
					<select name="wpeae_default_status" id="wpeae_default_status">
						<option value="publish" <?php if ($cur_wpeae_default_status == "publish"): ?>selected="selected"<?php endif; ?>>publish</option>
						<option value="draft" <?php if ($cur_wpeae_default_status == "draft"): ?>selected="selected"<?php endif; ?>>draft</option>
					</select> 						
				</td>
			</tr>
		</table>
		
		<h3>Schedule setting</h3>
		<table class="form-table">
			<tr valign="top">
				<th scope="row" class="titledesc"><label for="wpeae_price_auto_update">Update price</label></th>
				<td class="forminp forminp-text"><input type="checkbox" id="wpeae_price_auto_update" name="wpeae_price_auto_update" value="yes" <?php if (get_option('wpeae_price_auto_update', false)): ?>checked<?php endif; ?>/></td>
			</tr>
			
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="wpeae_price_auto_update_period">Update price period</label>
				</th>
				<td class="forminp forminp-select">
					<?php $cur_wpeae_price_auto_update_period = get_option('wpeae_price_auto_update_period', 'daily'); ?>
					<select name="wpeae_price_auto_update_period" id="wpeae_price_auto_update_period" <?php if (!get_option('wpeae_price_auto_update', false)): ?>disabled<?php endif; ?>>
						<option value="hourly" <?php if ($cur_wpeae_price_auto_update_period == "hourly"): ?>selected="selected"<?php endif; ?>>hourly</option>
						<option value="twicedaily" <?php if ($cur_wpeae_price_auto_update_period == "twicedaily"): ?>selected="selected"<?php endif; ?>>twicedaily</option>
						<option value="daily" <?php if ($cur_wpeae_price_auto_update_period == "daily"): ?>selected="selected"<?php endif; ?>>daily</option>
					</select> 						
				</td>
			</tr>
		</table>
		
		
		<input class="button-primary" type="submit" value="Save settings"/><br/>
	</form>
</div>

<script>
	jQuery("#wpeae_use_source_shipment").change(function () {
		jQuery("#wpeae_default_shipment_charges").prop('disabled', jQuery(this).is(':checked'));
		return true;
	});
	
	jQuery("#wpeae_price_auto_update").change(function () {
		jQuery("#wpeae_price_auto_update_period").prop('disabled', !jQuery(this).is(':checked'));
		return true;
	});
	
</script>