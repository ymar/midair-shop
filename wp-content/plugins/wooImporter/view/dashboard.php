<div>
    <?php if (!$this->api->is_instaled()): ?>
        <?php $this->api->print_promo_page(); ?>
    <?php else: ?>
        <?php if ($this->api): ?>
            <h1><?php echo $this->api->get_config_value("dashboard_title") ? $this->api->get_config_value("dashboard_title") : $this->api->get_type(); ?></h1>
        <?php endif; ?>

        <?php $this->prepare_items(); ?>

        <?php settings_errors('wpeae_account_error'); ?>

        <?php if ($this->show_dashboard): ?>
            <h2>Search Filter</h2>

            <form id="wpeae-search-form" method="GET">
                <input type="hidden" name="page" id="page" value="<?php echo ((isset($_GET['page'])) ? $_GET['page'] : ''); ?>" />
                <input type="hidden" id="reset" name="reset" value="" />

                <table class="form-table">
                    <tbody>
                        <?php $filters = $this->api->get_filters(); ?>
                        <?php foreach ($filters as $filter_id => $filter): ?> 
                            <tr>
                                <th>
                                    <?php if (isset($filter['config']['label'])): ?>
                                        <label for="<?php echo is_array($filter['name']) ? reset($filter['name']) : $filter['name']; ?>">
                                            <?php echo $filter['config']['label']; ?>:
                                        </label>
                                    <?php endif; ?>
                                </th>
                                <td>
                                    <?php if (isset($filter['config']['type']) && $filter['config']['type'] == 'select'): ?>
                                        <select id="<?php echo $filter['name']; ?>" name="<?php echo $filter['name']; ?>" class="<?php echo isset($filter['config']['class']) ? $filter['config']['class'] : ""; ?>" style="<?php echo isset($filter['config']['style']) ? $filter['config']['style'] : ""; ?>">
                                            <?php if (is_array($filter['config']['data_source'])): ?>
                                                <?php foreach ($filter['config']['data_source'] as $c): ?>
                                                    <option class="level_<?php echo $c["level"]; ?>" value="<?php echo $c["id"]; ?>"<?php if (isset($this->filter[$filter['name']]) && $this->filter[$filter['name']] == $c["id"]): ?> selected<?php endif; ?>><?php echo $c["name"]; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    <?php else: ?>
                                        <?php if (is_array($filter['name'])): ?>
                                            <?php foreach ($filter['name'] as $nn): ?>
                                                <?php if (isset($filter['config'][$nn]['label'])): ?>
                                                    <label for="<?php echo $nn; ?>"><?php echo $filter['config'][$nn]['label']; ?></label>
                                                <?php endif; ?>
                                                <input name="<?php echo $nn; ?>" id="<?php echo $nn; ?>" 
                                                       placeholder="<?php echo isset($filter['config'][$nn]['placeholder']) ? $filter['config'][$nn]['placeholder'] : ""; ?>" 
                                                       value="<?php echo isset($this->filter[$nn]) ? $this->filter[$nn] : (isset($filter['config'][$nn]['default']) ? $filter['config'][$nn]['default'] : "") ?>" 
                                                       class="small-text" type="text"/>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <input name="<?php echo $filter['name']; ?>" id="<?php echo $filter['name']; ?>" 
                                                   placeholder="<?php echo isset($filter['config']['placeholder']) ? $filter['config']['placeholder'] : ""; ?>" 
                                                   value="<?php echo isset($this->filter[$filter['name']]) ? $this->filter[$filter['name']] : (isset($filter['config']['default']) ? $filter['config']['default'] : "") ?>" 
                                                   class="regular-text" type="text"/>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                    <?php if (isset($filter['config']['description'])): ?>    
                                        <span class="description"><?php echo $filter['config']['description']; ?></span>
                                    <?php endif; ?>
                                </td>

                            </tr>

                            <?php if ($filter['config']['dop_row']): ?>
                                <tr><th colspan="2"><?php echo $filter['config']['dop_row']; ?></th></tr>
                            <?php endif; ?>

                        <?php endforeach; ?>

                    </tbody>
                </table>

                <h2>Link to category</h2>

                <table class="form-table">
                    <tbody>

                        <tr>
                            <th><label for="category_id">Category:</label></th>
                            <td>
                                <select id="link_category_id" name="link_category_id" class="category_list" style="width:25em;">
                                    <option value=""></option>
                                    <?php foreach ($this->link_categories as $c): ?>
                                        <option value="<?php echo $c["term_id"]; ?>"<?php if (isset($this->filter["link_category_id"]) && $this->filter["link_category_id"] == $c["term_id"]): ?> selected<?php endif; ?>>
                                            <?php
                                            for ($i = 1; $i < $c["level"]; $i++) {
                                                echo "-";
                                            }
                                            ?>
                                            <?php echo $c["name"]; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>


                <?php if ($this->loader->has_account()) : ?>
                    <input type="button" id="wpeae-do-filter" class="button button-primary" value="Search"/><br/>
                    <p>
                        <?php settings_errors('wpeae_goods_list'); ?>
                    </p>
                    <h2>Products list</h2>
                    <div id="wpeae-goods-table">
                        <div class='import_process_loader'></div>
                        <?php $this->display(); ?>
                    </div>

                    <?php add_thickbox(); ?>

                <?php endif; ?>
            </form>
        <?php endif; ?>
    <?php endif; ?>
</div>
<?php if ($this->api->is_instaled() && $this->show_dashboard): ?>
    <div id="upload_image_dlg" style="display: none">
        <div>
            <form id="image_upload_form" method="post" action="#" enctype="multipart/form-data" >
                <input type='hidden' value='<?php echo wp_create_nonce('upload_thumb'); ?>' name='_nonce' />
                <input type="hidden" name="upload_product_id" id="upload_product_id" value=""/>
                <input type="hidden" name="action" id="action" value="wpeae_upload_image"/>
                <input type="file" name="upload_image" id="upload_image"/>
                <br/><br/>
                <input id="submit-ajax" name="submit-ajax" type="submit" value="Upload this Image" class="button button-primary"/> <span id="upload_progress"></span>
            </form>
        </div>
    </div>

    <div id="edit_desc_dlg" style="display: none"></div>
<?php endif; ?>
<div class="wpeae_module_version">Module version: <?php echo $this->api->get_config_value("version"); ?></div>