<?php

/*
  Plugin Name: WooCommerce Importer
  Description: It`s a plugin that used to import products from eBay, Aliexpress, Amazon, Walmart to your Wordpress WooCommerce site. The plugin is helpful to create a store with specific products and use affiliate URLs.
  Version: 2.0.1.2
  Author: Geometrix
  License: GPLv2+
  Author URI: http://gmetrixteam.com
 */

if (!defined('WPEAE_ROOT_URL')) {
    define('WPEAE_ROOT_URL', plugin_dir_url(__FILE__));
}
if (!defined('WPEAE_ROOT_PATH')) {
    define('WPEAE_ROOT_PATH', plugin_dir_path(__FILE__));
}
if (!defined('WPEAE_FILE_FULLNAME')) {
    define('WPEAE_FILE_FULLNAME', __FILE__);
}

include_once(dirname(__FILE__) . '/include.php');
include_once(dirname(__FILE__) . '/schedule.php');
include_once dirname(__FILE__) . '/install.php';

if (!class_exists('WooImporter')):

    class WooImporter {

        function __construct() {
            add_action('admin_menu', array($this, 'add_menu'));
            add_action('admin_enqueue_scripts', array($this, 'add_assets'));

            register_activation_hook(__FILE__, array($this, 'install'));
            register_deactivation_hook(__FILE__, array($this, 'uninstall'));
        }

        public function add_assets($page) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            $plugin_data = get_plugin_data(__FILE__);

            wp_enqueue_style('wpeae-style', plugins_url('assets/css/style.css', __FILE__), array(), $plugin_data['Version']);
            wp_enqueue_style('wpeae-font-style', plugins_url('assets/css/font-awesome.min.css', __FILE__), array(), $plugin_data['Version']);
            wp_enqueue_style('wpeae-dtp-style', plugins_url('assets/js/datetimepicker/jquery.datetimepicker.css', __FILE__), array(), $plugin_data['Version']);

            wp_enqueue_script('wpeae-script', plugins_url('assets/js/script.js', __FILE__), array(), $plugin_data['Version']);
            wp_enqueue_script('wpeae-dtp-script', plugins_url('assets/js/datetimepicker/jquery.datetimepicker.js', __FILE__), array(), $plugin_data['Version']);
        }

        function add_menu() {
            new WPEAE_Goods();
            $api_list = wpeae_get_api_list();

            $root_menu_id = "wpeae-dashboard";


            //find default api 
            $default_api = "";
            foreach ($api_list as $api) {
                if ($api->is_instaled()) {
                    $default_api = $api;
                    break;
                }
            }

            $root_menu_id_top = $root_menu_id . ($default_api ? ("-" . $default_api->get_type()) : "");


            add_menu_page(WPEAE_NAME, WPEAE_NAME, 'manage_options', $root_menu_id_top, '', plugins_url('images/logo.png', __FILE__));

            // include installed api

            foreach ($api_list as $api) {
                if ($api->is_instaled()) {

                    $title = $api->get_config_value("menu_title") ? $api->get_config_value("menu_title") : $api->get_type();
                    add_submenu_page($root_menu_id_top, $title, $title, 'manage_options', $root_menu_id . "-" . $api->get_type(), array(new WPEAE_DashboardPage($api->get_type()), 'render'));
                }
            }
            // include not installed api
            foreach ($api_list as $api) {
                if (!$api->is_instaled()) {
                    $title = $api->get_config_value("menu_title") ? $api->get_config_value("menu_title") : $api->get_type();
                    add_submenu_page($root_menu_id_top, $title, '<span class="promo">' . $title . '</span>', 'manage_options', $root_menu_id . "-" . $api->get_type(), array(new WPEAE_DashboardPage($api->get_type()), 'render'));
                }
            }
            add_submenu_page($root_menu_id_top, WPEAE_NAME . ' Settings', 'Settings', 'manage_options', 'wpeae-settings', array(new WPEAE_SettingsPage(), 'render'));
        }

        function install() {
            wpeae_install();
        }

        function uninstall() {
            wpeae_uninstall();
        }

    }

endif;

new WooImporter();
