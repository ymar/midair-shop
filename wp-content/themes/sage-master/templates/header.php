<!-- Fixed navbar -->
     <nav class="navbar navbar-default navbar-fixed-top">
       <?php
  // Fix menu overlap bug..
  if ( is_admin_bar_showing() ) echo '<div style="min-height: 28px;"></div>';
?>
       <div class="container">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
         </div>
         <div id="navbar" class="navbar-collapse collapse">
           <?php
           if (has_nav_menu('primary_navigation')) :
             wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
           endif;
           ?>
             </li>
           </ul>

         </div><!--/.nav-collapse -->
       </div>
     </nav>
